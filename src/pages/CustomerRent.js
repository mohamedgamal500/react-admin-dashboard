import { Helmet } from "react-helmet";
import { Box, Container, Grid, Pagination } from "@material-ui/core";
import ProductCard from "src/components/product//ProductCard";
import products from "src/__mocks__/products";
import axios from "axios";
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
const CustomerRent = () => {
  const [productss, setProducts] = useState([
    { title: "customer car", id: 3289432 },
  ]);

  const { id } = useParams();

  console.log(id);

  useEffect(() => {
    // axios.get(`https://api/customer?id=${id}`).then((response) => setProducts(response));
  }, []);
  return (
    <>
      <Helmet>
        <title>Products | Material Kit</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: "background.default",
          minHeight: "100%",
          py: 3,
        }}
      >
        <Container maxWidth={false}>
          <Box sx={{ pt: 3 }}>
            <Grid container spacing={3}>
              {productss.map((product) => (
                <Grid item key={product.id} lg={4} md={6} xs={12}>
                  <ProductCard product={product} />
                </Grid>
              ))}
            </Grid>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              pt: 3,
            }}
          ></Box>
        </Container>
      </Box>
    </>
  );
};

export default CustomerRent;
