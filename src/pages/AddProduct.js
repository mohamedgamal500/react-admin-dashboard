import { Link as RouterLink, useNavigate } from "react-router-dom";
import { Helmet } from "react-helmet";
import * as Yup from "yup";
import { Formik } from "formik";
import {
  Box,
  Button,
  Container,
  TextField,
  Typography,
} from "@material-ui/core";
import axios from "axios";

const AddProduct = () => {
  const navigate = useNavigate();

  return (
    <>
      <Helmet>
        <title>Add Car | Material Kit</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: "background.default",
          display: "flex",
          flexDirection: "column",
          height: "100%",
          justifyContent: "center",
        }}
      >
        <Container maxWidth="sm">
          <Formik
            initialValues={{
              name: "",
              model: "",
              plateNumber: "",
              price: "",
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().max(255).required("Name is required"),
              model: Yup.string().max(255).required("Model is required"),
              price: Yup.string().max(255).required("Model is required"),
              plateNumber: Yup.string()
                .max(255)
                .required("plateNumber is required"),
            })}
            onSubmit={(values) => {
              //axios.post("https://", values).then((response) => {});
              navigate("/app/products", { replace: true });
              console.log("product", values);
            }}
          >
            {({
              errors,
              handleBlur,
              handleChange,
              handleSubmit,
              isSubmitting,
              touched,
              values,
            }) => (
              <form onSubmit={handleSubmit}>
                <Box sx={{ mb: 3 }}>
                  <Typography color="textPrimary" variant="h2">
                    Add Car
                  </Typography>
                </Box>

                <TextField
                  error={Boolean(touched.name && errors.name)}
                  fullWidth
                  helperText={touched.name && errors.name}
                  label="Name"
                  margin="normal"
                  name="name"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="text"
                  value={values.name}
                  variant="outlined"
                />
                <TextField
                  error={Boolean(touched.model && errors.model)}
                  fullWidth
                  helperText={touched.model && errors.model}
                  label="model"
                  margin="normal"
                  name="model"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.model}
                  variant="outlined"
                />

                <TextField
                  error={Boolean(touched.plateNumber && errors.plateNumber)}
                  fullWidth
                  helperText={touched.plateNumber && errors.plateNumber}
                  label="plate number"
                  margin="normal"
                  name="plateNumber"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.plateNumber}
                  variant="outlined"
                />

                <TextField
                  error={Boolean(touched.price && errors.price)}
                  fullWidth
                  helperText={touched.price && errors.price}
                  margin="normal"
                  label="price"
                  name="price"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="text"
                  value={values.price}
                  variant="outlined"
                />

                <Box
                  sx={{
                    alignItems: "center",
                    display: "flex",
                    ml: -1,
                  }}
                ></Box>
                <Box sx={{ py: 2 }}>
                  <Button
                    color="primary"
                    disabled={isSubmitting}
                    fullWidth
                    size="large"
                    type="submit"
                    variant="contained"
                  >
                    Add New Car
                  </Button>
                </Box>
              </form>
            )}
          </Formik>
        </Container>
      </Box>
    </>
  );
};

export default AddProduct;
