import { Navigate } from "react-router-dom";
import DashboardLayout from "src/components/DashboardLayout";
import MainLayout from "src/components/MainLayout";
//import Account from "src/pages/Account";
import CustomerList from "src/pages/CustomerList";
//import Dashboard from "src/pages/Dashboard";
import Login from "src/pages/Login";
import NotFound from "src/pages/NotFound";
import ProductList from "src/pages/ProductList";
import Register from "src/pages/Register";
//import Settings from "src/pages/Settings";
import AddProduct from "./pages/AddProduct";
import CustomerRent from "./pages/CustomerRent";
const routes = [
  {
    path: "app",
    element: <DashboardLayout />,
    children: [
      //{ path: "account", element: <Account /> },
      { path: "customers", element: <CustomerList /> },
      { path: "customers/rent/:id", element: <CustomerRent /> },
      //{ path: "dashboard", element: <Dashboard /> },
      { path: "products", element: <ProductList /> },

      { path: "products/add", element: <AddProduct /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
  {
    path: "/",
    element: <MainLayout />,
    children: [
      { path: "login", element: <Login /> },
      { path: "register", element: <Register /> },
      { path: "404", element: <NotFound /> },
      { path: "/", element: <Navigate to="/login" /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
];

export default routes;
